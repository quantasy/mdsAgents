/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.mdsagents.led;

import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LEDFrame;
import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LEDStripDeviceConfig;
import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LEDStripV2ServiceContract;
import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LedStripIntent;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 *
 * @author reto
 */
public class Check {

    public static void main(String[] args) throws JsonProcessingException {
        LEDStripDeviceConfig config = new LEDStripDeviceConfig(LEDStripDeviceConfig.ChipType.WS2801, 2000000, 1000, 200, LEDStripDeviceConfig.ChannelMapping.RGB);
        LEDStripV2ServiceContract contract = new LEDStripV2ServiceContract("FCG");
        LEDFrame frame = new LEDFrame(config.chipType.numberOfChannels, config.numberOfLEDs, Integer.valueOf(config.frameDurationInMilliseconds));
        for (int i = 0; i < frame.getNumberOfLEDs(); i++) {
            frame.setColor(0, i, 140);
            frame.setColor(1, i, 70);
            frame.setColor(2, i, 10);
            
            
        }
        LedStripIntent intent=new LedStripIntent();
        intent.config=config;
        intent.LEDFrame=frame;
        System.out.println(contract.getObjectMapper().writeValueAsString(intent));

    }
}
