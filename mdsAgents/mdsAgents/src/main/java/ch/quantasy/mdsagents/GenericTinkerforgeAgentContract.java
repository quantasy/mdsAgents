/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.mdsagents;

import ch.quantasy.mdservice.message.Message;
import ch.quantasy.mdsmqtt.gateway.client.contract.AyamlServiceContract;
import java.util.Map;

/**
 *
 * @author reto
 */
public class GenericTinkerforgeAgentContract extends AyamlServiceContract{

    public GenericTinkerforgeAgentContract(String rootContext, String baseClass) {
        super("Agent",rootContext, baseClass);
    }

    @Override
    public void setMessageTopics(Map<String, Class<? extends Message>> messageTopicMap) {
        //Nothing
    }
    
    
}
