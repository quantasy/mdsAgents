package ch.quantasy.mdsagents.led.abilities;

import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LEDFrame;
import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LEDStripDeviceConfig;
import ch.quantasy.gateway.binding.tinkerforge.ledStrip.LEDStripServiceContract;
import ch.quantasy.mdsmqtt.gateway.client.MQTTGatewayClient;
import java.util.ArrayList;
import java.util.List;

public class ColidingDots extends AnLEDAbility {

    private final List<LEDFrame> frames;

    public ColidingDots(MQTTGatewayClient MQTTGatewayClient, LEDStripServiceContract ledServiceContract, LEDStripDeviceConfig config) {
        super(MQTTGatewayClient, ledServiceContract, config);
        frames = new ArrayList<>();
    }

    public void run() {
        super.setLEDFrame(getNewLEDFrame());

        LEDFrame leds = super.getNewLEDFrame();
        LEDFrame flash = super.getNewLEDFrame();

        for (int position = 0; position < flash.getNumberOfLEDs(); position++) {
            for (int channel = 0; channel < flash.getNumberOfChannels(); channel++) {
                flash.setColor(channel, position, (short) 255);
            }
            flash.durationInMillis=10;
        }

        //for (int i = 0; i < leds.getNumberOfChannels(); i++) {
        int i = 0;
        leds.setColor((short) i, (short) 0, (short) 255);
        leds.setColor(i + 1, (short) 0, (short) 255);
        //}
        try {
            LEDFrame newLEDs = super.getNewLEDFrame();
            while (true) {
                while (frames.size() < 300) {
                    //for (int channel = 0; channel < leds.getNumberOfChannels(); channel++) {
                    int channel = 0;
                    for (int position = 1; position < leds.getNumberOfLEDs(); position++) {
                        newLEDs.setColor(channel, position, leds.getColor(channel, position - 1));
                        newLEDs.setColor((channel + 1), position - 1, leds.getColor(channel + 1, position));
                    }
                    newLEDs.setColor((short) channel, 0, leds.getColor(channel, leds.getNumberOfLEDs() - 1));
                    newLEDs.setColor((short) (channel + 1), leds.getNumberOfLEDs() - 1, leds.getColor(channel + 1, 0));
                    newLEDs.durationInMillis=10;

                    //}
                    LEDFrame tmpLEDs = leds;
                    leds = newLEDs;
                    newLEDs = tmpLEDs;
                    frames.add(new LEDFrame(leds));
                    if (leds.getColor(0, leds.getNumberOfLEDs() / 2) != 0) {
                        frames.add(flash);
                    }
                }
                super.setLEDFrames(frames);
                frames.clear();

                Thread.sleep(getConfig().frameDurationInMilliseconds * 50);

                synchronized (this) {
                    while (getCounter() > 100) {
                        this.wait(getConfig().frameDurationInMilliseconds * 1000);
                    }
                }
            }
        } catch (InterruptedException ex) {
            super.setLEDFrame(getNewLEDFrame());
        }
    }
}
