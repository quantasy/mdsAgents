/*
 *   "mdsMapper"
 *
 *    mdsMapper(tm): A gateway to provide universal mapping (alias) ability.
 *
 *    Copyright (c) 2019 Bern University of Applied Sciences (BFH),
 *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *    Quellgasse 21, CH-2501 Biel, Switzerland
 *
 *    Licensed under Dual License consisting of:
 *    1. GNU Affero General Public License (AGPL) v3
 *    and
 *    2. Commercial license
 *
 *
 *    1. This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *     accordance with the commercial license agreement provided with the
 *     Software or, alternatively, in accordance with the terms contained in
 *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *
 *
 *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *
 *
 */
package ch.quantasy.mdsagents.mapper;

import ch.quantasy.mdsMapper.MDSMapperGateway;
import ch.quantasy.mdsMapper.gateway.binding.mapper.TargetConfig;
import ch.quantasy.mdsMapper.gateway.binding.mapper.MapperIntent;
import ch.quantasy.mdsMapper.gateway.binding.mapper.MapperServiceContract;
import ch.quantasy.mdsMapper.gateway.binding.mapper.SourceConfig;
import ch.quantasy.mdsMapper.gateway.binding.mapper.ConditionConfig;
import ch.quantasy.mdservice.message.Message;
import ch.quantasy.mdsmqtt.gateway.client.MQTTGatewayClient;
import ch.quantasy.mdsmqtt.gateway.client.contract.AyamlServiceContract;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author reto
 */
public class MDSMapperAgent {

    public static String computerName;

    static {
        try {
            computerName = java.net.InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            Logger.getLogger(MDSMapperGateway.class.getName()).log(Level.SEVERE, null, ex);
            computerName = "undefined";
        }
    }

    public MDSMapperAgent(MQTTGatewayClient client) {
        MapperServiceContract mapperContract = new MapperServiceContract(computerName);

        MapperIntent intent = new MapperIntent();
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "T1", "Home/Pool/Pumpe",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "T1", "1", true, "Timer/Tick/U/+/S/UNIXEpoch/hoursSinceMidnight", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "T1", "1", "1", true, "$.[?(@['value'] >= 08 && @['value'] <= 21)]","Zeit zwischen 9Uhr und 22Uhr"));
        
         intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "T2", "Home/Pool/Pumpe",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "T2", "1", true, "Timer/Tick/U/+/S/UNIXEpoch/hoursSinceMidnight", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "T2", "1", "1", true, "$.[?(@['value'] < 08 || @['value'] > 21)]","Zeit zwischen 23Uhr und 9Uhr"));
        
        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "P1", "Home/Pool/Pumpe",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "P1", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P1", "1", "1", true, "$.[?(@['address']=='16934980')]","Schalter 21"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P1", "1", "2", true, "$.[?(@['unit']==13)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P1", "1", "3", true, "$.[?(@['switchTo']>0)]", "on"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "P2", "Home/Pool/Pumpe",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "P2", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P2", "1", "1", true, "$.[?(@['address']=='16934980')]","Schalter 21"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P2", "1", "2", true, "$.[?(@['unit']==13)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P2", "1", "3", true, "$.[?(@['switchTo']==0)]", "off"));
       
        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "PI1", "TF/RemoteSwitch/U/qD7/I", Boolean.TRUE, Boolean.TRUE, mapperContract.getDocumentFor("switchSocketCParameters:\n  switchingValue: switchOn\n  systemCode: L\n  deviceCode: 2\n"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "PI1", "1", Boolean.TRUE, "Home/Pool/Pumpe", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "PI1", "1", "1", Boolean.TRUE, "$.message.[?(@['request']=='switchOn')]", "Pool-Pumpe"));
        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "PI2", "TF/RemoteSwitch/U/qD7/I", Boolean.TRUE, Boolean.TRUE, mapperContract.getDocumentFor("switchSocketCParameters:\n  switchingValue: switchOff\n  systemCode: L\n  deviceCode: 2\n"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "PI2", "1", Boolean.TRUE, "Home/Pool/Pumpe", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "PI2", "1", "1", Boolean.TRUE, "$.message.[?(@['request']=='switchOff')]", "Pool-Pumpe"));

        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "P3", "Home/Pool/Gegenstrom",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "P3", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P3", "1", "1", true, "$.[?(@['address']=='16934980')]","Schalter 21"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P3", "1", "2", true, "$.[?(@['unit']==5)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P3", "1", "3", true, "$.[?(@['switchTo']>0)]", "on"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "P4", "Home/Pool/Gegenstrom",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "P4", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P4", "1", "1", true, "$.[?(@['address']=='16934980')]","Schalter 21"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P4", "1", "2", true, "$.[?(@['unit']==5)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "P4", "1", "3", true, "$.[?(@['switchTo']==0)]", "off"));
       
        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "PI3", "TF/RemoteSwitch/U/qD7/I", Boolean.TRUE, Boolean.TRUE, mapperContract.getDocumentFor("switchSocketCParameters:\n  switchingValue: switchOn\n  systemCode: L\n  deviceCode: 1\n"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "PI3", "1", Boolean.TRUE, "Home/Pool/Gegenstrom", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "PI4", "1", "1", Boolean.TRUE, "$.message.[?(@['request']=='switchOn')]", "Pool-Gegenstrom"));
        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "PI4", "TF/RemoteSwitch/U/qD7/I", Boolean.TRUE, Boolean.TRUE, mapperContract.getDocumentFor("switchSocketCParameters:\n  switchingValue: switchOff\n  systemCode: L\n  deviceCode: 1\n"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "PI4", "1", Boolean.TRUE, "Home/Pool/Gegenstrom", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "PI4", "1", "1", Boolean.TRUE, "$.message.[?(@['request']=='switchOff')]", "Pool-Gegenstrom"));

        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "B1", "Home/Storen/OG/Links",false, true, mapperContract.getDocumentFor("request: up"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "B1", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B1", "1", "1", true, "$.[?(@['address']=='21355980')]","Schalter 20"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B1", "1", "2", true, "$.[?(@['unit']==13)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B1", "1", "3", true, "$.[?(@['switchTo']>0)]", "up"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "B2", "Home/Storen/OG/Links",false, true, mapperContract.getDocumentFor("request: down"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "B2", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B2", "1", "1", true, "$.[?(@['address']=='21355980')]","Schalter 20"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B2", "1", "2", true, "$.[?(@['unit']==13)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B2", "1", "3", true, "$.[?(@['switchTo']==0)]", "down"));
        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "B3", "Home/Storen/OG/Rechts",false, true, mapperContract.getDocumentFor("request: up"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "B3", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B3", "1", "1", true, "$.[?(@['address']=='21355980')]","Schalter 20"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B3", "1", "2", true, "$.[?(@['unit']==5)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B3", "1", "3", true, "$.[?(@['switchTo']>0)]", "up"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "B4", "Home/Storen/OG/Rechts",false, true, mapperContract.getDocumentFor("request: down"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "B4", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B4", "1", "1", true, "$.[?(@['address']=='21355980')]","Schalter 20"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B4", "1", "2", true, "$.[?(@['unit']==5)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "B4", "1", "3", true, "$.[?(@['switchTo']==0)]", "down"));
        
 
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "BI1", "TF/RemoteSwitch/U/jKE/I", Boolean.TRUE, Boolean.TRUE, mapperContract.getDocumentFor("switchSocketBParameters:\n  switchingValue: 0\n  address: 3\n  unit: 1\n"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "BI1", "1", Boolean.TRUE, "Home/Storen/OG/Rechts", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "BI1", "1", "1", Boolean.TRUE, "$.message.[?(@['request']=='up')]", "Store OG Rechts"));
        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "BI2", "TF/RemoteSwitch/U/jKE/I", Boolean.TRUE, Boolean.TRUE, mapperContract.getDocumentFor("switchSocketBParameters:\n  switchingValue: 1\n  address: 3\n  unit: 1\n"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "BI2", "1", Boolean.TRUE, "Home/Storen/OG/Rechts", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "BI2", "1", "1", Boolean.TRUE, "$.message.[?(@['request']=='down')]", "Store OG Rechts"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "BI3", "TF/RemoteSwitch/U/jKE/I", Boolean.TRUE, Boolean.TRUE, mapperContract.getDocumentFor("switchSocketBParameters:\n  switchingValue: 0\n  address: 3\n  unit: 3\n"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "BI3", "1", Boolean.TRUE, "Home/Storen/OG/Links", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "BI3", "1", "1", Boolean.TRUE, "$.message.[?(@['request']=='up')]", "Store OG Links"));
        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "BI4", "TF/RemoteSwitch/U/jKE/I", Boolean.TRUE, Boolean.TRUE, mapperContract.getDocumentFor("switchSocketBParameters:\n  switchingValue: 1\n  address: 3\n  unit: 3\n"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "BI4", "1", Boolean.TRUE, "Home/Storen/OG/Links", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "BI4", "1", "1", Boolean.TRUE, "$.message.[?(@['request']=='down')]", "Store OG Links"));
       
        
        
        
        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L1", "Home/Licht/OG/Gallerie",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L1", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L1", "1", "1", true, "$.[?(@['address']=='25323942')]","Schalter 17"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L1", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L1", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L2", "Home/Licht/OG/Gallerie",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "2", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L2", "1", "1", true, "$.[?(@['address']=='33202008')]", "Schalter 18"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L2", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L2", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L3", "Home/Licht/OG/Gallerie",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L3", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L3", "1", "1", true, "$.[?(@['address']=='23064012')]", "Schalter 19"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L3", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L3", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L4", "Home/Licht/OG/Gallerie",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L4", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L4", "1", "1", true, "$.[?(@['address']=='25323942')]", "Schalter 17"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L4", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L4", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L5", "Home/Licht/OG/Gallerie",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L5", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L5", "1", "1", true, "$.[?(@['address']=='33202008')]", "Schalter 18"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L5", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L5", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L6", "Home/Licht/OG/Gallerie",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L6", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L6", "1", "1", true, "$.[?(@['address']=='23064012')]", "Schalter 19"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L6", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L6", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L7", "Home/Licht/OG/Bad",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L7", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L7", "1", "1", true, "$.[?(@['address']=='25323942')]", "Schalter 17"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L7", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L7", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L8", "Home/Licht/OG/Bad",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L8", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L8", "1", "1", true, "$.[?(@['address']=='25323942')]", "Schalter 17"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L8", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L8", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L9", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L9", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L9", "1", "1", true, "$.[?(@['address']=='24487590')]", "Schalter 12"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L9", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L9", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L10", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L10", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L10", "1", "1", true, "$.[?(@['address']=='26571724')]", "Schalter 2"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L10", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L10", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L10", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L10", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L10", "1", "1", true, "$.[?(@['address']=='32802694')]", "Schalter 1"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L10", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L10", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L11", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L11", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L11", "1", "1", true, "$.[?(@['address']=='24487590')]", "Schalter 12"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L11", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L11", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L12", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L12", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L12", "1", "1", true, "$.[?(@['address']=='26571724')]", "Schalter 2"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L12", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L12", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L13", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L13", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L13", "1", "1", true, "$.[?(@['address']=='32802694')]", "Schalter 1"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L13", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L13", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L14", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L14", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L14", "1", "1", true, "$.[?(@['address']=='24487590')]", "Schalter 12"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L14", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L14", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L15", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L15", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L15", "1", "1", true, "$.[?(@['address']=='26571724')]", "Schalter 2"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L15", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L15", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L16", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L16", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L16", "1", "1", true, "$.[?(@['address']=='32802694')]", "Schalter 1"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L16", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L16", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L17", "Home/Licht/EG/Wohnzimmer",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L17", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L17", "1", "1", true, "$.[?(@['address']=='24487590')]", "Schalter 12"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L17", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L17", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L18", "Home/Licht/EG/Wohnzimmer",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L18", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L18", "1", "1", true, "$.[?(@['address']=='24487590')]", "Schalter 12"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L18", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L18", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L19", "Home/Licht/UG/Musik",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L19", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L19", "1", "1", true, "$.[?(@['address']=='26571724')]", "Schalter 2"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L19", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L19", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L20", "Home/Licht/UG/Musik",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L20", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L20", "1", "1", true, "$.[?(@['address']=='26571724')]", "Schalter 2"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L20", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L20", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L21", "Home/Licht/UG/Sport",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L21", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L21", "1", "1", true, "$.[?(@['address']=='32802694')]", "Schalter 1"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L21", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L21", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L22", "Home/Licht/UG/Sport",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L22", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L22", "1", "1", true, "$.[?(@['address']=='32802694')]", "Schalter 1"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L22", "1", "2", true, "$.[?(@['unit']==13)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L22", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L25", "Home/Licht/OG/Dusche",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L25", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L25", "1", "1", true, "$.[?(@['address']=='23064012')]", "Schalter 16"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L25", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L25", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L26", "Home/Licht/OG/Dusche",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L26", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L26", "1", "1", true, "$.[?(@['address']=='23064012')]", "Schalter 16"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L26", "1", "2", true, "$.[?(@['unit']==5)]", "a"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L26", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L27", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L27", "1", true, "WebView/RemoteSwitch/E/touched/remoteSwitch/+", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L27", "1", "1", true, "$.[?(@['type']=='switchSocketB')]", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L27", "1", "2", true, "$.[?(@['floor']=='UG')]", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L27", "1", "3", true, "$.[?(@['address']==2)]" , null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L27", "1", "4", true, "$.[?(@['unit']==0)]", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L27", "1", "5", true, "$.[?(@['switchingValue']=='switchOff')]", null));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L28", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L28", "1", true, "WebView/RemoteSwitch/E/touched/remoteSwitch/+", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L28", "1", "1", true, "$.[?(@['type']=='switchSocketB')]", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L28", "1", "2", true, "$.[?(@['floor']=='UG')]", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L28", "1", "3", true, "$.[?(@['address']==2)]", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L28", "1", "4", true, "$.[?(@['unit']==0)]", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L28", "1", "5", true, "$.[?(@['switchingValue']=='switchOn')]", null));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L29", "Home/Licht/UG/Gang",false, true, mapperContract.getDocumentFor("request: dimTo\nvalue: $/dimValue"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L29", "1", true, "WebView/RemoteSwitch/E/touched/remoteSwitch/+", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L29", "1", "1", true, "$.[?(@['type']=='switchSocketB')]", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L29", "1", "2", true, "$.[?(@['floor']=='UG')]", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L29", "1", "3", true, "$.[?(@['address']==2)]", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L29", "1", "4", true, "$.[?(@['unit']==0)]", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L29", "1", "5", true, "$.dimValue", "dimValue exists"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L30", "Home/Licht/EG/Büro",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L30", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L30", "1", "1", true, "$.[?(@['address']=='18551756')]" , "Schalter 12"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L30", "1", "2", true, "$.[?(@['unit']==5)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L30", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L31", "Home/Licht/EG/Büro",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L31", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/B", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L31", "1", "1", true, "$.[?(@['address']=='18551756')]", "Schalter 12"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L31", "1", "2", true, "$.[?(@['unit']==5)]", "b"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L31", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L32", "Home/Licht/EG/Dusche",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L32", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/C", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L32", "1", "1", true, "$.[?(@['systemCode']=='C')]", "Schalter 12"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L32", "1", "2", true, "$.[?(@['deviceCode']==1)]", "c"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L32", "1", "3", true, "$.[?(@['switchTo']>0)]", "ein"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L33", "Home/Licht/EG/Dusche",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L33", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/C", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L33", "1", "1", true, "$.[?(@['systemCode']=='C')]", "Schalter 12"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L33", "1", "2", true, "$.[?(@['deviceCode']==1)]", "c"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L33", "1", "3", true, "$.[?(@['switchTo']==0)]", "aus"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L34", "Home/Licht/EG/Kammer",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L34", "1", true, "TF/MotionDetector/U/kfT/E/motionDetected", null));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L35", "Home/Licht/EG/Kammer",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L35", "1", true, "Timer/Tick/U/smarthome01/E/tick/lights/cupboard", null));

        //The following is:
        //This AND This...
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L36", "Home/Licht/EG/Carport",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L36", "1", true, "TF/AmbientLight/U/jxr/E/illuminance/reached", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L36", "1", "1", true, "$.[?(@['value']<100)]", "Dunkel"));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L36", "2", true, "TF/MotionDetector/U/kgB/E/eventDetectionCycle", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L36", "2", "1", true, "$.[?(@['value']==true)]", "Bewegung"));

//        //...OR...
//        //This AND This
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L37", "Home/Licht/EG/Carport",false, true, mapperContract.getDocumentFor("request: switchOn"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L37", "1", true, "TF/AmbientLight/U/jxr/E/illuminance/reached", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L37", "1", "1", true, "$.[?(@['value']<100)]", "Dunkel"));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L37", "2", true, "TF/MotionDetector/U/kfP/E/eventDetectionCycle", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L37", "2", "1", true, "$.[?(@['value']==true)]", "Bewegung"));

//        //The following is:
//        
//        //This...AND...This
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "L38", "Home/Licht/EG/Carport",false, true, mapperContract.getDocumentFor("request: switchOff"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L38", "1", true, "TF/MotionDetector/U/kgB/E/eventDetectionCycle", null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "L38", "2", true, "TF/MotionDetector/U/kfP/E/eventDetectionCycle", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L38", "1", "1", true, "$.[?(@['value']==false)]", "Keine Bewegung mehr"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "L38", "2", "1", true, "$.[?(@['value']==false)]", "Keine Bewegung mehr"));
//
//        intent = new MapperIntent();
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "F1", "Home/Fenster/EG/Büro",false, true, mapperContract.getDocumentFor("value: opened"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "F1", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/C", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F1", "1", "1", true, "$.[?(@['systemCode']=='A')]", "Fenster 1"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F1", "1", "2", true, "$.[?(@['deviceCode']==1)]", "Fenster 1"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F1", "1", "3", true, "$.[?(@['switchTo']==1)]", "offen"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "F2", "Home/Fenster/EG/Büro",false, true, mapperContract.getDocumentFor("value: closed"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "F2", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/C", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F2", "1", "1", true, "$.[?(@['systemCode']=='A')]", "Fenster 1"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F2", "1", "2", true, "$.[?(@['deviceCode']==1)]", "Fenster 1"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F2", "1", "3", true, "$.[?(@['switchTo']==0)]", "geschlossen"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "F3", "Home/Fenster/UG/Musik",false, true, mapperContract.getDocumentFor("value: opened"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "F3", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/C", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F3", "1", "1", true, "$.[?(@['systemCode']=='B')]", "Fenster 2"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F3", "1", "2", true, "$.[?(@['deviceCode']==1)]", "Fenster 2"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F3", "1", "3", true, "$.[?(@['switchTo']==1)]", "offen"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "F4", "Home/Fenster/UG/Musik",false, true, mapperContract.getDocumentFor("value: closed"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "F4", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/C", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F4", "1", "1", true, "$.[?(@['systemCode']=='B')]", "Fenster 2"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F4", "1", "2", true, "$.[?(@['deviceCode']==1)]", "Fenster 2"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F4", "1", "3", true, "$.[?(@['switchTo']==0)]", "geschlossen"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "F5", "Home/Fenster/EG/Wohnzimmer",false, true, mapperContract.getDocumentFor("value: opened"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "F5", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/C", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F5", "1", "1", true, "$.[?(@['systemCode']=='C')]", "Fenster 3"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F5", "1", "2", true, "$.[?(@['deviceCode']==4)]", "Fenster 3"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F5", "1", "3", true, "$.[?(@['switchTo']==1)]", "offen"));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "F6", "Home/Fenster/EG/Wohnzimmer",false, true, mapperContract.getDocumentFor("value: closed"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "F6", "1", true, "TF/RemoteSwitchV2/U/+/E/switch/C", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F6", "1", "1", true, "$.[?(@['systemCode']=='C')]", "Fenster 3"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F6", "1", "2", true, "$.[?(@['deviceCode']==4)]", "Fenster 3"));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "F6", "1", "3", true, "$.[?(@['switchTo']==0)]", "geschlossen"));

        client.readyToPublish(mapperContract.INTENT, intent);

    }

    public static void main(String[] args) throws Exception {
        //URI mqttURI = URI.create("tcp://smarthome01:1883");
        URI mqttURI = URI.create("tcp://127.0.0.1:1883");

        MQTTGatewayClient client = new MQTTGatewayClient(mqttURI, "24t49hq349f83h", new AyamlServiceContract("Tester", "Test") {
            @Override
            public void setMessageTopics(Map<String, Class<? extends Message>> messageTopicMap) {
            }
        });
        MDSMapperAgent mapper = new MDSMapperAgent(client);
        client.connect();
        System.in.read();
        client.disconnect();
    }
}
