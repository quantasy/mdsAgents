/*
 *   "TiMqWay"
 *
 *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *
 *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *    Quellgasse 21, CH-2501 Biel, Switzerland
 *
 *    Licensed under Dual License consisting of:
 *    1. GNU Affero General Public License (AGPL) v3
 *    and
 *    2. Commercial license
 *
 *
 *    1. This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *     accordance with the commercial license agreement provided with the
 *     Software or, alternatively, in accordance with the terms contained in
 *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *
 *
 *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *
 *
 */
package ch.quantasy.mdsagents.led.abilities;

import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LEDFrame;
import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LEDStripDeviceConfig;
import ch.quantasy.gateway.binding.tinkerforge.ledStrip.LEDStripServiceContract;
import ch.quantasy.mdsmqtt.gateway.client.MQTTGatewayClient;
import java.util.ArrayList;
import java.util.List;
import org.openimaj.image.MBFImage;

/**
 * This LEDAbility prepares 150 frames where each frame has another led lit. It
 * keeps checking that the LEDStrip-service does not underrun hence as soon as
 * the LEDStrip-service publishes a stock-value lower than 100, the next pile of
 * frames is sent.
 *
 * @author reto
 */
public class ImageToStripe extends AnLEDAbility {

    private final List<LEDFrame> frames;
    private final MBFImage image;

    public ImageToStripe(MQTTGatewayClient MQTTGatewayClient, LEDStripServiceContract ledServiceContract, LEDStripDeviceConfig config, MBFImage image) {
        super(MQTTGatewayClient, ledServiceContract, config);
        frames = new ArrayList<>();
        this.image = image;
    }
    private int i;

    public void run() {
        super.blackOut();
        byte[] red = image.getBand(0).toByteImage();
        byte[] green = image.getBand(1).toByteImage();
        byte[] blue = image.getBand(2).toByteImage();

        try {
            while (true) {
                LEDFrame leds = super.getNewLEDFrame();
                int pixelCount = 0;
                while (pixelCount < red.length) {
                    for (int i = 0; i < leds.getNumberOfLEDs() && pixelCount < red.length; i++) {
                        leds.setColor(0, i, red[pixelCount]);
                        leds.setColor(1, i, green[pixelCount]);
                        leds.setColor(2, i, blue[pixelCount]);
                        pixelCount++;

                    }
                    frames.add(new LEDFrame(leds));
                }
                super.setLEDFrames(frames);
                frames.clear();

                Thread.sleep(super.getConfig().frameDurationInMilliseconds * 50);

                synchronized (this) {
                    while (getCounter() > 100) {
                        this.wait(super.getConfig().frameDurationInMilliseconds * 1000);
                    }
                }
            }

//            for (int j = 0; j < 170; j++) {
//                for (int i = 0; i < leds.getNumberOfLEDs(); i++) {
//                    leds.setColor(3, i, j);
//
//                }
//                frames.add(new LEDFrame(leds));
//            }
//
//            LEDFrame frame = new LEDFrame(leds);
//            frame.durationInMillis = 10000;
//            frames.add(frame);
//            super.setLEDFrames(frames);
//
//            frames.clear();
        } catch (InterruptedException ex) {
            LEDFrame f=getNewLEDFrame();
            f.durationInMillis=100000;
            super.setLEDFrame(f);
        }
        frames.clear();
    }
}
